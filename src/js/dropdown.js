export const dropdownHandler = () => {
  const selected = document.querySelectorAll('#select-box__btn');
  const selectBoxInput = document.querySelector('input[class="select-box__input ingredients"]');
  const selectContainer = document.querySelectorAll('#select-box');
  const searchOptions = document.querySelectorAll('.option-search');
  const options = document.querySelectorAll('.option-container__item');

  selected.forEach(s => {
    s.addEventListener('click', (e) => {
      console.log(e);
      let optionsContainer = e.target.parentNode.nextElementSibling;
      let searchOption = e.target.previousElementSibling;
      optionsContainer.classList.toggle("active");
      searchOption.value = '';   
        if (optionsContainer.classList.contains('active')) {
          searchOption.focus();
        }
    });
  });

  options.forEach(o => {
    o.addEventListener('click', (e) => {
      console.log(e.target.innerHTML);
      console.log(selectBoxInput);
      selectBoxInput.innerHTML = e.target.innerHTML;
      //selectContainer.classList.remove('active');
    });
  });
}



import { recipes } from './recipes';

export const getAllRecipes = () => {
  return recipes;
}

export const getAllIngredients = () => {
  const allIngredients = [];
  recipes.map(recipe => {
    recipe.ingredients.map(i => {
      allIngredients.push(i.ingredient.toLowerCase());
    });
  });
  return allIngredients;
}

export const getAllEquipments = () => {
  const allEquipments = [];
  recipes.map(recipe => {
    allEquipments.push(recipe.appliance.toLowerCase());
  });
  return allEquipments;
}

export const getAllUstensils = () => {
  const allUstensils = [];
  recipes.map(recipe => {
    recipe.ustensils.map(u => {
      allUstensils.push(u.toLowerCase());
    });
  });
  return allUstensils;
}

export const getRecipeById = id => {
  return recipes[id];
}

export const getRecipeSearchTermsById = id => {
  let recipe = recipes[id];
  const name = [recipe.name];
  const ingredients = recipe.ingredients.map(i => {
    return i.ingredient;
  });
  const descriptionString = recipe.description;
  const description = descriptionString.split(' ');
  const equipment = [recipe.appliance];
  const ustensils = recipe.ustensils.map(u => {
    return u;
  });
  return [...name, ...ingredients, description, ...equipment, ...ustensils];
}

export const recipeCard = () =>{
  let template = recipes.map(recipe => {
    return `
      <div class="card" style="width: 18rem;">
        <img src="https://via.placeholder.com/380x180" class="card-img-top" alt="...">
        <div class="card-header">
          <h5 class="card-title">${recipe.name}</h5>
          <i class="far fa-clock"></i>
          <p>${recipe.time} min</p>
        </div>   
        <div class="card-body">
          <ul class="ingredients">
            ${recipe.ingredients.map(ingredient =>`<li>${ingredient.ingredient}: ${ingredient.quantity ? ingredient.quantity : ''}${ingredient.unit ? ingredient.unit : ''}</li>`).join('')}
          </ul>
          <p class="card-description">${recipe.description}</p>
        </div>
      </div>
    `;
  });
  return template;
}